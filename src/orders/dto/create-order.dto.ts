import { IsNotEmpty } from 'class-validator';
class CreateOrderItemDto {
  productId: number;
  amount: number;
}
export class CreateOrderDto {
  [x: string]: any;
  @IsNotEmpty()
  customerId: number;

  @IsNotEmpty()
  orderItem: CreateOrderItemDto[];
}
